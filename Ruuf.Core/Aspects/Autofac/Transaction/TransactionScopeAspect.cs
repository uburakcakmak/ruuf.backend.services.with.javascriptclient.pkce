﻿using System.Transactions;
using Castle.DynamicProxy;
using Ruuf.Core.Utilities.Interceptors;

namespace Ruuf.Core.Aspects.Autofac.Transaction
{
    public class TransactionScopeAspect : MethodInterception
    {
        public override void Intercept(IInvocation invocation)//metodun yaşam döngüsünde kontrol edilir
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    invocation.Proceed();//metodu çalıştırmaya çalış eğer başarılı olursa complete et, hata aldıysa yapılan işlemleri geri al ve hata fırlat diyoruz catch bloğuyla
                    transactionScope.Complete();
                }
                catch (System.Exception e)
                {
                    transactionScope.Dispose();
                    throw;
                }
            }
        }
    }
}
