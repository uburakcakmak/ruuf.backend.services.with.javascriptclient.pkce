﻿using Castle.DynamicProxy;
using Ruuf.Core.CrossCuttingConcerns.Validation;
using Ruuf.Core.Utilities.Interceptors;
using Ruuf.Core.Utilities.Messages;
using FluentValidation;

namespace Ruuf.Core.Aspects.Autofac.Validation
{
    public class ValidationAspect : MethodInterception
    {
        private Type _validatorType;
        public ValidationAspect(Type validatorType)
        {
            if (!typeof(IValidator).IsAssignableFrom(validatorType))//gönderilen validatortype bir IValidator türünde değilse
            {
                throw new System.Exception(Messages.WrongValidationType);
            }

            _validatorType = validatorType;
        }

        //bu validation aspect onbefore'da daha metod çalışmadan önce çalışacağı için onbefore'u override edip kodumuzu yazarız
        protected override void OnBefore(IInvocation invocation)
        {
            var validator = (IValidator)Activator.CreateInstance(_validatorType);//reflection yöntemiyle bir instance üretiyor
            var entityType = _validatorType.BaseType.GetGenericArguments()[0];//generic argümanın ilk parametresini yani product'ı al
            var entities = invocation.Arguments.Where(t => t.GetType() == entityType);//git metodun argümanlarına bak
            foreach (var entity in entities)
            {
                ValidationTool.Validate(validator, entity);
            }
        }
    }
}
