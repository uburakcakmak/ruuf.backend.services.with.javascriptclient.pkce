﻿using Castle.DynamicProxy;
using Ruuf.Core.CrossCuttingConcerns.Caching;
using Ruuf.Core.Utilities.Interceptors;
using Ruuf.Core.Utilities.IoC;
using Microsoft.Extensions.DependencyInjection;

namespace Ruuf.Core.Aspects.Autofac.Caching
{
    public class CacheRemoveAspect : MethodInterception
    {
        private string _pattern;
        private ICacheManager _cacheManager;

        public CacheRemoveAspect(string pattern)
        {
            _pattern = pattern;
            _cacheManager = ServiceTool.ServiceProvider.GetService<ICacheManager>();
        }

        protected override void OnSuccess(IInvocation invocation)
        {
            _cacheManager.RemoveByPattern(_pattern);
        }
    }
}
