﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System.Text;

namespace Ruuf.Core.CrossCuttingConcerns.Caching.Redis
{
    public class RedisCacheManager : ICacheManager
    {
        private readonly IDistributedCache _distributedCache;
        public RedisCacheManager(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }

        public async Task<T> GetFromCacheOrDB<T>(string cacheKey, int cacheExpirationInMinutes, Func<Task<T>> func)
        {
            string json;

            var data = await _distributedCache.GetAsync(cacheKey);

            if (data != null)
            {
                var dataString = Encoding.UTF8.GetString(data);
                return JsonConvert.DeserializeObject<T>(dataString);
            }
            else
            {
                var result = await func.Invoke();

                if (result != null)
                {
                    var option = new DistributedCacheEntryOptions();

                    if (cacheExpirationInMinutes > 0)
                    {
                        option.SetSlidingExpiration(TimeSpan.FromMinutes(cacheExpirationInMinutes));
                        option.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(cacheExpirationInMinutes);
                    }
                    json = JsonConvert.SerializeObject(result);
                    data = Encoding.UTF8.GetBytes(json);
                    await _distributedCache.SetAsync(cacheKey, data, option);
                }

                return result;
            }
        }

        public async Task ClearCacheForKeys(List<string> cacheKeys)
        {
            foreach (var cacheKey in cacheKeys)
            {
                await _distributedCache.RemoveAsync(cacheKey);
            }
        }

        public T Get<T>(string key)
        {
            throw new NotImplementedException();
        }

        public async Task<object> Get(string key)
        {
            return await _distributedCache.GetAsync(key);
        }

        public void Add(string key, object data, int duration)
        {
            throw new NotImplementedException();
        }

        public bool IsAdd(string key)
        {
            throw new NotImplementedException();
        }

        public void Remove(string key)
        {
            throw new NotImplementedException();
        }

        public void RemoveByPattern(string pattern)
        {
            throw new NotImplementedException();
        }
    }
}