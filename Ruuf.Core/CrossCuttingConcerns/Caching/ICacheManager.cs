﻿
namespace Ruuf.Core.CrossCuttingConcerns.Caching
{
    public interface ICacheManager
    {
        Task<T> GetFromCacheOrDB<T>(string cacheKey, int cacheExpirationInMinutes, Func<Task<T>> func);
        Task ClearCacheForKeys(List<string> cacheKeys);

        T Get<T>(string key);
        Task<object> Get(string key);
        void Add(string key, object data, int duration);
        bool IsAdd(string key);
        void Remove(string key);
        void RemoveByPattern(string pattern);
    }
}
