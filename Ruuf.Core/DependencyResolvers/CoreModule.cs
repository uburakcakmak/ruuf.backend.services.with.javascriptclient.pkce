﻿using System.Diagnostics;
using Ruuf.Core.Utilities.IoC;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Ruuf.Core.CrossCuttingConcerns.Caching;
using Ruuf.Core.CrossCuttingConcerns.Caching.Redis;
using System.Globalization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Ruuf.Core.Utilities.Helpers;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Ruuf.Core.DependencyResolvers
{
    public class CoreModule : ICoreModule
    {
        public void Load(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();// aspnetcore.http nuget paketi core ve business katmanlarına yüklenir
            //services.AddMemoryCache();
            services.AddSingleton<ICacheManager, RedisCacheManager>();
            services.AddSingleton<Stopwatch>();


            #region Localization
            var cultureEN = CultureInfo.CreateSpecificCulture("en-US");
            cultureEN.DateTimeFormat = new DateTimeFormatInfo
            {
                ShortDatePattern = "dd.MM.yyyy",
                LongDatePattern = "dd.MM.yyyy hh:mm:ss tt"
            };
            var cultureTR = CultureInfo.CreateSpecificCulture("tr-TR");
            cultureTR.DateTimeFormat = new DateTimeFormatInfo
            {
                ShortDatePattern = "dd.MM.yyyy",
                LongDatePattern = "dd.MM.yyyy hh:mm:ss tt"
            };

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures =
                    new List<CultureInfo>
                                    {
                            cultureEN,
                            cultureTR,
                                    };
                options.DefaultRequestCulture = new RequestCulture("tr-TR");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });
            #endregion
        }

    }
}
