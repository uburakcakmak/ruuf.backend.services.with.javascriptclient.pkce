﻿
using Microsoft.EntityFrameworkCore;
using Ruuf.Entities.Concrete.IdentityServer;

namespace Ruuf.Core.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void SeedRoles(this ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Role>()
                 .HasData(
                     new Role { Name = "Admin", NormalizedName = "Admin" },
                     new Role { Name = "Reviewer", NormalizedName = "Reviewer" },
                     new Role { Name = "Investor", NormalizedName = "Investor" },
                     new Role { Name = "ApplicantForHome", NormalizedName = "ApplicantForHome" }
                 );
        }
    }
}
