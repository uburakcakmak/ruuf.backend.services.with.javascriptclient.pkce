﻿using Ruuf.Core.Utilities.IoC;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System.Reflection;
using Microsoft.Extensions.Configuration;

namespace Ruuf.Core.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDependencyResolvers(this IServiceCollection services,
            ICoreModule[] modules)
        {
            foreach (var module in modules)
            {
                module.Load(services);
            }

            return ServiceTool.Create(services);
        }

        public static IServiceCollection AddRuufSwaggerGen(
            this IServiceCollection services,
            OpenApiInfo openApiInfo,
            OpenApiContact openApiContact)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(openApiInfo.Version, new OpenApiInfo
                {
                    Version = openApiInfo.Version,
                    Title = openApiInfo.Title,
                    Description = openApiInfo.Description,
                    Contact = new OpenApiContact
                    {
                        Name = openApiContact.Name,
                        Email = openApiContact.Email,
                        Url = openApiContact.Url,
                    }
                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please enter JWT with Bearer into field",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Id = "Bearer",
                                Type = ReferenceType.SecurityScheme
                            }
                        },
                        new List<string>()
                    }
                });


                // Expose XML comments in Swagger UI
                var entryAssembly = Assembly.GetEntryAssembly();
                if (entryAssembly != null)
                {
                    var xmlFile = $"{entryAssembly.GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    c.IncludeXmlComments(xmlPath);
                }
            });

            return services;
        }

        /// <summary>
        /// Same as the services.Configure, but it combines two blocks in settings provider
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <typeparam name="TRuufServiceSettings"></typeparam>
        /// <returns></returns>
        public static IServiceCollection ConfigureRuufServiceSettings<T>(this IServiceCollection services, IConfiguration configuration
        ) where T : class
        {
            services.Configure<T>(instance =>
            {
                configuration.GetSection(nameof(T)).Bind(instance);
            });
            return services;
        }

    }
}
