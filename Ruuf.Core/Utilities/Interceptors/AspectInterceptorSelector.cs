﻿using System.Reflection;
using Castle.DynamicProxy;

namespace Ruuf.Core.Utilities.Interceptors
{
    /// <summary>
    /// araya girmek için postsharp da kullanılabilir ama ücretli olduğu için reflectionları kullanarak kendi interceptor'ımızı yazdık.
    /// </summary>
    public class AspectInterceptorSelector : IInterceptorSelector
    {
        public IInterceptor[] SelectInterceptors(Type type, MethodInfo method, IInterceptor[] interceptors)
        {
            var classAttributes = type.GetCustomAttributes<MethodInterceptionBaseAttribute>
                (true).ToList();
            var methodAttributes = type.GetMethod(method.Name)
                .GetCustomAttributes<MethodInterceptionBaseAttribute>(true);
            classAttributes.AddRange(methodAttributes);
            //classAttributes.Add(new ExceptionLogAspect(typeof(FileLogger)));

            return classAttributes.OrderBy(x => x.Priority).ToArray();
        }
    }
}
