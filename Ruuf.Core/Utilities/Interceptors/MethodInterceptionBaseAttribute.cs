﻿using Castle.DynamicProxy;

namespace Ruuf.Core.Utilities.Interceptors
{
    /// <summary>
    /// tüm aspectlerde bunu kullanabiliriz. castle dynamicproxy nuget paketi yüklenir
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]//bu attribute class ve metodlarda kullanılabilir, birden fazla kullanılabilir ve inherit edildiği alt classlarda kullanılabilir demek
    public abstract class MethodInterceptionBaseAttribute : Attribute, IInterceptor
    {
        public int Priority { get; set; }// çalışma sırasını belirlememizi sağlar. önce cache mi, validasyon mu gibi

        public virtual void Intercept(IInvocation invocation)//virtual olması bu metodun değiştirilebilir olmasını sağlar
        {

        }
    }
}
