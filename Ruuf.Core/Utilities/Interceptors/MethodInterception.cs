﻿using Castle.DynamicProxy;

namespace Ruuf.Core.Utilities.Interceptors
{
    public abstract class MethodInterception:MethodInterceptionBaseAttribute
    {
        protected virtual void OnBefore(IInvocation invocation) { }// metod çalışmadan önce çalış demek. invocation çalıştırılmaya çalışılan operasyon demek
        protected virtual void OnAfter(IInvocation invocation) { }
        protected virtual void OnException(IInvocation invocation,System.Exception e) { }
        protected virtual void OnSuccess(IInvocation invocation) { }// metod başarılıysa çalış demek
       
        //metodun nasıl çalışacağını aşağıda belirtiyoruz
        public override void Intercept(IInvocation invocation)
        {
            var isSuccess = true;
            OnBefore(invocation);//onbeforeda bişey varsa invocationu çalıştır. sonra aşağıdakini çalıştır
            try
            {
                invocation.Proceed();
            }
            catch (Exception e)
            {
                isSuccess = false;
                OnException(invocation,e);//hata varsa onexceptionu çalıştır
                throw;
            }
            finally
            {
                if (isSuccess)
                    OnSuccess(invocation);// başarılıysa onsuccess çalıştır
            }
            OnAfter(invocation);//metod bittiğinde on afterı çalıştır
        }
    }
}
