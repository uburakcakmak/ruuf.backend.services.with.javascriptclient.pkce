﻿
namespace Ruuf.Core.Utilities.Configurations
{
    public class ApiConfigurations
    {
        public string MySqlConnStr { get; set; }
        public string RedisConnStr { get; set; }
        public string ApplicationUrl { get; set; }
        public string AuthApiAuthority { get; set; }
        public string SelfApiName { get; set; }
        public string AllowedCORSOrigins { get; set; }
        public string TokenScopes { get; set; }
        public string AuthClientId { get; set; }
        public string AuthClientSecret { get; set; }
    }
}