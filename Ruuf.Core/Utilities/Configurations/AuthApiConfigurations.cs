﻿using IdentityServer4.Models;
using IdentityServer4.Test;

namespace Ruuf.Core.Utilities.Configurations
{
    public class AuthApiConfigurations
    {

        public string MySqlConnStr { get; set; }
        public string BaseUrl { get; set; }

        public List<IdentityResource> IdentityResources { get; set; }
        public List<ApiResource> ApiResources { get; set; }
        public List<ApiScope> ApiScopes { get; set; }
        public List<Client> Clients { get; set; }
        public List<TestUser> TestUsers { get; set; }
    }
}