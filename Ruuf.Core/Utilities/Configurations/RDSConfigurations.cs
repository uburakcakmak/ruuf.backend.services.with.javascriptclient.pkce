﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ruuf.Core.Utilities.Configurations
{
    public class RDSConfigurations
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Engine { get; set; }
        public string Host { get; set; }
        public long Port { get; set; }
        public string DbInstanceIdentifier { get; set; }
    }
}
