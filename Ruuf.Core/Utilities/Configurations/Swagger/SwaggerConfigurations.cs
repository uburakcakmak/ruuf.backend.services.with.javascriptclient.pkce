﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ruuf.Core.Utilities.Configurations.Swagger
{
    public class SwaggerConfigurations
    {
        public string SwaggerName { get; set; }

        public SwaggerDocument SwaggerDoc { get; set; }

        public SwaggerUI UseSwaggerUI { get; set; }
    }
    public class SwaggerDocument
    {
        public string Version { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public SwaggerContact Contact { get; set; }

    }
    public class SwaggerContact
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public Uri Url { get; set; }
    }
    public class SwaggerUI
    {
        public string SwaggerUIEndpoint { get; set; }
        public string UIName { get; set; }
    }
}
