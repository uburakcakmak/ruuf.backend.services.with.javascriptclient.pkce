﻿using System.Security.Cryptography;

namespace Ruuf.Core.Utilities.Helpers
{
    public static class RandomGenerator
    {
        private static string UpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private static string LowerCase = "abcdefghijklmnopqrstuvwxyz";
        private static string Digits = "0123456789";

        /// <summary>
        /// random büyük harf ve rakam içeren string döndürür
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string UpperCaseAndNumber(int length = 10)
        {
            var random = new Random();
            string chars = $"{UpperCase}{Digits}";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string Base64(int arraySize = 32)
        {
            byte[] number = new byte[arraySize];
            using (var random = RandomNumberGenerator.Create())
            {
                random.GetBytes(number);
                return Convert.ToBase64String(number);
            }
        }
    }
}
