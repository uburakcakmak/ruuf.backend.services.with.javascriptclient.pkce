﻿using Amazon;
using Amazon.SecretsManager;
using Amazon.SecretsManager.Model;
using Ruuf.Core.Utilities.Configurations;
using System.Text.Json;

namespace Ruuf.Core.Utilities.Helpers
{
    public static class AWSSecretHelper
    {
        public static string GetDBConnString()
        {
            var secretName = "arn:aws:secretsmanager:eu-central-1:936225507148:secret:prod/ruuf/registrar-gJqZTx";
            var region = "eu-central-1";

            var memoryStream = new MemoryStream();
            var client = new AmazonSecretsManagerClient(RegionEndpoint.GetBySystemName(region));
            var request = new GetSecretValueRequest();

            request.SecretId = secretName;
            request.VersionStage = "AWSCURRENT"; // VersionStage defaults to AWSCURRENT if unspecified.

            try
            {
                var response = client.GetSecretValueAsync(request).Result;
                if (response.SecretString != null)
                {
                    var secret = JsonSerializer.Deserialize<RDSConfigurations>(response.SecretString, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
                    return $"server={secret.Host}; port={secret.Port}; database=ruuf; user={secret.Username}; password={secret.Password}; Persist Security Info=False; Connect Timeout=300";
                }
                else
                {
                    throw new ArgumentException("Secret was not a string");
                }
            }
            catch (Exception e)
            {
                // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
                // Deal with the exception here, and/or rethrow at your discretion.
                throw new FailedToFetchAWSSecret("Failed to fetch AWS secret", e);
            }
        }
    }

    // TODO: Error handling mekanizması kuracağız. burayı kaldırırız o zaman
    public class FailedToFetchAWSSecret : Exception 
    {
        public FailedToFetchAWSSecret()
        {
        }

        public FailedToFetchAWSSecret(string message)
            : base(message)
        {
        }

        public FailedToFetchAWSSecret(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
