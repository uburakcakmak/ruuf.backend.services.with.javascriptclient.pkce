﻿using IdentityModel.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ruuf.Core.Utilities.Configurations;
using Ruuf.Entities.Concrete.IdentityServer;
using Ruuf.Entities.Concrete.RequestModels;
using System.Text;
using static IdentityModel.OidcConstants;

namespace Ruuf.Core.Utilities.Helpers
{
    public static class OAuthHelper
    {
        public static async Task<(bool isSuccessful, JObject tokenResponse, string error, string errorDescription)> RequestResourceOwnerPasswordToken(
            IHttpClientFactory clientFactory,
            ApiConfigurations apiConfigurations,
            UserLoginRequest loginRequest)
        {
            var client = clientFactory.CreateClient();
            var disco = await client.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
            {
                Address = apiConfigurations.AuthApiAuthority,
                Policy = { RequireHttps = false }
            });

            if (disco.IsError)
                return (false, null, disco.Error, null);

            var tokenResponse = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "",
                ClientSecret = "",
                UserName = loginRequest.Email,
                Password = loginRequest.Password,
                Scope = apiConfigurations.TokenScopes
            });

            if (tokenResponse.IsError)
                return (false, null, tokenResponse.Error, tokenResponse.ErrorDescription);

            return (true, tokenResponse.Json, null, null);
        }

        public static async Task<(bool isSuccessful, JObject tokenResponse, string error, string errorDescription)> RequestRefreshToken(
            IHttpClientFactory clientFactory,
            ApiConfigurations apiConfigurations,
            RenewAccessTokenRequest renewAccessTokenRequest)
        {
            var client = clientFactory.CreateClient();
            var disco = await client.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
            {
                Address = apiConfigurations.AuthApiAuthority,
                Policy = { RequireHttps = false }
            });

            if (disco.IsError)
                return (false, null, disco.Error, null);

            var tokenResponse = await client.RequestRefreshTokenAsync(new RefreshTokenRequest
            {
                Address = disco.TokenEndpoint,
                Scope = apiConfigurations.TokenScopes,
                ClientId = renewAccessTokenRequest.ClientId,
                ClientSecret = renewAccessTokenRequest.ClientSecret,
                RefreshToken = renewAccessTokenRequest.RefreshToken
            });

            if (tokenResponse.IsError)
                return (false, null, tokenResponse.Error, tokenResponse.ErrorDescription);

            return (true, tokenResponse.Json, null, null);
        }

        public static async Task<(bool isSuccessful, string error)> RevokeRefreshToken(
           IHttpClientFactory clientFactory,
           ApiConfigurations apiConfigurations,
           TokenRevocationRequest tokenRevocationRequest)
        {
            var client = clientFactory.CreateClient();
            var disco = await client.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
            {
                Address = apiConfigurations.AuthApiAuthority,
                Policy = { RequireHttps = false }
            });

            if (disco.IsError)
                return (false, disco.Error);

            var tokenResponse = await client.RevokeTokenAsync(new TokenRevocationRequest
            {
                Address = disco.RevocationEndpoint,
                ClientId = apiConfigurations.AuthClientId,
                ClientSecret = apiConfigurations.AuthClientSecret,
                Token = tokenRevocationRequest.Token,
                TokenTypeHint = GrantTypes.RefreshToken
            });

            if (tokenResponse.IsError)
                return (false, tokenResponse.Error);

            return (true, null);
        }

        public static async Task<(bool isSuccessful, string error)> RequestRuufRegisterUser(
           IHttpClientFactory clientFactory,
           ApiConfigurations apiConfigurations,
           RegisterUserRequest registerUserRequest)
        {
            var client = clientFactory.CreateClient();

            var myContent = JsonConvert.SerializeObject(registerUserRequest);
            var stringContent = new StringContent(myContent, UnicodeEncoding.UTF8, "application/json");
            var result = await client.PostAsync(apiConfigurations.AuthApiAuthority + "/api/Account/RegisterUser", stringContent);

            if (result.IsSuccessStatusCode)
                return (false, result.Content.ToString());

            return (true, null);
        }

        public static async Task<IdentityModel.Client.TokenResponse> RequestAuthorizationCodeToken(
           IHttpClientFactory clientFactory,
           ApiConfigurations apiConfigurations,
           UserLoginRequest loginRequest)
        {
            var client = clientFactory.CreateClient();
            var disco = await client.GetDiscoveryDocumentAsync(apiConfigurations.AuthApiAuthority);

            var tokenResponse = await client.RequestAuthorizationCodeTokenAsync(new AuthorizationCodeTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "ruuf.web.spa.client",
                //ClientSecret = "",
                Code = "",
                RedirectUri = "https://app.com/callback",
                GrantType = GrantTypes.AuthorizationCode,
                // optional PKCE parameter
                CodeVerifier = "xyz"
            });

            return tokenResponse ?? null;
        }
    }
}