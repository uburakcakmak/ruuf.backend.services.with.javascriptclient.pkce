﻿using Microsoft.Extensions.DependencyInjection;

namespace Ruuf.Core.Utilities.IoC
{
    public interface ICoreModule
    {
        void Load(IServiceCollection collection);
    }
}
