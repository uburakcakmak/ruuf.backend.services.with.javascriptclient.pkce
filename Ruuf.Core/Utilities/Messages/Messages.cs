﻿
namespace Ruuf.Core.Utilities.Messages
{
    public static class Messages
    {
        public static string WrongValidationType = "Wrong Validation Type";
        public static string WrongLoggerType = "Wrong Logger Type";

        public static string UserNotFound = "Kullanıcı bulunamadı";
        public static string PasswordError = "Şifre hatalı";
        public static string SuccessfulLogin = "Sisteme giriş başarılı";
        public static string UserAlreadyExists = "Bu kullanıcı zaten mevcut";
        public static string UserRegistered = "Kullanıcı başarıyla kaydedildi";
        public static string AccessTokenCreated = "Access token başarıyla oluşturuldu";
        public static string ReferralCodeNotExists = "Hatalı referans kodu";
        public static string WrongApplicationType = "Hatalı başvuru türü seçildi";
        public static string UserRegisterError = "Kullanıcı eklenirken hata oluştu";

        public static string AuthorizationDenied = "Yetkiniz yok";
        public static string InvalidRefreshToken = "Geçersiz refresh token!";
    }
}
