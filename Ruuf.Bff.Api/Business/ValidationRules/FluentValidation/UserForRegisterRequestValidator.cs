﻿using FluentValidation;
using Ruuf.Bff.Api.Business.Constants;
using Ruuf.Entities.Concrete.RequestModels;

namespace Ruuf.Bff.Api.Business.ValidationRules.FluentValidation
{
    //class veya abstract class inherit edilir
    public class UserForRegisterRequestValidator : AbstractValidator<RegisterUserRequest>
    {
        private static int _minimumLength = 8;
        public UserForRegisterRequestValidator()
        {
            RuleFor(user => user.Email)
                .EmailAddress().WithMessage(Messages.InvalidEmailAddress)
                .NotEmpty().WithMessage(Messages.EmailIsRequired);

            RuleFor(user => user.Password).NotEmpty().WithMessage(Messages.PasswordEmpty)
                                        .MinimumLength(_minimumLength).WithMessage(Messages.PasswordLength)
                                        .Matches("[A-Z]").WithMessage(Messages.PasswordUppercaseLetter)
                                        .Matches("[a-z]").WithMessage(Messages.PasswordLowercaseLetter)
                                        .Matches("[0-9]").WithMessage(Messages.PasswordDigit)
                                        .Matches("[^a-zA-Z0-9]").WithMessage(Messages.PasswordSpecialCharacter);

            RuleFor(user => user.FullName)
               .NotEmpty().WithMessage(Messages.FullNameIsRequired);

            RuleFor(user => user.ApplicationType)
                .IsInEnum().WithMessage(Messages.WrongApplicationType);
        }

    }
}
