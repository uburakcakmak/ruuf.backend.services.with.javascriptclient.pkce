﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using Castle.DynamicProxy;
using Ruuf.Bff.Api.Business.Abstract;
using Ruuf.Bff.Api.Business.Concrete;
using Ruuf.Bff.Api.DataAccess.Abstract;
using Ruuf.Bff.Api.DataAccess.Concrete.EntityFramework;
using Ruuf.Core.Utilities.Interceptors;

namespace Ruuf.Bff.Api.Business.DependencyResolvers.Autofac
{
    public class AutofacBusinessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserManager>().As<IUserService>();
            builder.RegisterType<ApplicationManager>().As<IApplicationService>();

            builder.RegisterType<EfLoginHistoryDal>().As<ILoginHistoryDal>();
            builder.RegisterType<EfInvestorApplicationDal>().As<IInvestorApplicationDal>();

            #region aspectlerin devreye girebilmesi için. autofac ioc container'ı üzerinden çalıştırıyoruz.
            //bu operasyonların başında sonunda başarılı olduğunda çalışmasını sağlamak için aşağıdaki konfigürasyon yapılır
            ////autofac.extras.dynamicproxy nuget paketi core ve business katmanlarına yüklenir. şu nesneler için aşağıdaki interceptorı çalıştır diyoruz
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            builder.RegisterAssemblyTypes(assembly).AsImplementedInterfaces()// aop için interceptors çalıştıracaksın
                .EnableInterfaceInterceptors(new ProxyGenerationOptions()
                {
                    Selector = new AspectInterceptorSelector()// araya girecek objemizi veriyoruz
                }).SingleInstance();// tek bir aspect instance'ı oluştur

            #endregion
        }
    }
}
