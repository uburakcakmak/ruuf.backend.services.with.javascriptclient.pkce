﻿using Microsoft.Extensions.Options;
using Ruuf.Bff.Api.Business.Abstract;
using Ruuf.Bff.Api.Business.Constants;
using Ruuf.Bff.Api.DataAccess.Abstract;
using Ruuf.Core.Extensions;
using Ruuf.Core.Utilities.Configurations;
using Ruuf.Core.Utilities.Helpers;
using Ruuf.Entities.Concrete;
using Ruuf.Entities.Concrete.IdentityServer;
using Ruuf.Entities.Concrete.RequestModels;
using Ruuf.Entities.Concrete.ViewModels;

namespace Ruuf.Bff.Api.Business.Concrete
{
    public class UserManager : IUserService
    {
        #region DIs

        private readonly ILoginHistoryDal _loginHistoryDal;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ApiConfigurations _bffApiSettings;

        public UserManager(
            ILoginHistoryDal loginHistoryDal,
            IHttpClientFactory httpClientFactory,
            IOptionsSnapshot<ApiConfigurations> bffApiSettings
            )
        {
            _loginHistoryDal = loginHistoryDal;
            _httpClientFactory = httpClientFactory;
            _bffApiSettings = bffApiSettings.Value;
        }

        #endregion

        ////[ValidationAspect(typeof(UserForRegisterRequestValidator), Priority = 1)]
        public async Task<string> RegisterUser(RegisterUserRequest registerUserRequest)
        {

            //var oAuthResponse =
            //   await OAuthHelper.RequestRuufRegisterUser(_httpClientFactory, _bffApiSettings, registerUserRequest);

            //if (!oAuthResponse.isSuccessful)
            //    throw new Exception(Messages.UserRegisterError);

            return Messages.UserRegistered;
        }

        public async Task<TokenResultViewModel> Login(UserLoginRequest loginRequest)
        {
            //var oAuthResponse =
            //    await OAuthHelper.RequestRuufResourceOwnerPasswordToken(_httpClientFactory, _bffApiSettings, loginRequest);

            //var currentUserId = _currentUser.GetUserIdOrThrowUnauthorizedAccessException();

            //var loginHistory = new LoginHistory
            //{
            //    Id = Guid.NewGuid(),
            //    CreatedDate = DateTime.Now,
            //    IsLoginSuccessful = false,
            //    UserId = currentUserId,
            //    IsActive = false,
            //    IsMobileLogin = loginRequest.IsMobileLogin,
            //    IsWebLogin = loginRequest.IsWebLogin,
            //    DeviceIp = loginRequest.DeviceIp
            //};

            //if (!oAuthResponse.isSuccessful)
            //{
            //    loginHistory.IsLoginSuccessful = false;
            //    AddLoginHistory(loginHistory);
            //    throw new Exception(Messages.UserNotFound);
            //}

            //var response = new TokenResultViewModel()
            //{
            //    AccessToken = oAuthResponse.tokenResponse.GetValue("access_token")?.ToString(),
            //    AccessTokenExpiresIn = oAuthResponse.tokenResponse.GetValue("expires_in")?.ToString(),
            //    Scope = oAuthResponse.tokenResponse.GetValue("scope")?.ToString(),
            //    TokenType = oAuthResponse.tokenResponse.GetValue("token_type")?.ToString(),
            //    RefreshToken = oAuthResponse.tokenResponse.GetValue("refresh_token")?.ToString()
            //};

            //AddLoginHistory(loginHistory);

            //return response;
            return null;
        }

        public async Task Logout(string refreshToken)
        {
            //var currentUserId = _currentUser.GetUserIdOrThrowUnauthorizedAccessException();
            //var user = await _userDataUnit.GetAsync(currentUserId);

            //var response = await HelperOAuth.RevokeAksNextRefreshToken(_httpClientFactory, _appSettings, request.RefreshToken);

            //var logoutHistroyDto = new LogoutHistoryDto()
            //{
            //    DeviceBrand = request.DeviceBrand,
            //    DeviceModel = request.DeviceModel,
            //    DeviceUuid = request.DeviceUuid,
            //    IdentificationNo = user.IdentificationNumber,
            //    MobileNumber = user.MobileNumber,
            //    Error = response.error
            //};

            //await _logoutHistoryBusinessUnit.AddOrUpdateAsync(logoutHistroyDto);
        }

        public async Task<TokenResultViewModel> GetAccessTokenWithRefreshToken(RenewAccessTokenRequest renewAccessTokenRequest)
        {
            //var oAuthResponse =
            //     await OAuthHelper.RequestRuufRefreshToken(_httpClientFactory, _bffApiSettings, renewAccessTokenRequest);

            //if (!oAuthResponse.isSuccessful)
            //    throw new Exception(Messages.InvalidRefreshToken);

            //var response = new TokenResultViewModel()
            //{
            //    AccessToken = oAuthResponse.tokenResponse.GetValue("access_token")?.ToString(),
            //    AccessTokenExpiresIn = oAuthResponse.tokenResponse.GetValue("expires_in")?.ToString(),
            //    Scope = oAuthResponse.tokenResponse.GetValue("scope")?.ToString(),
            //    TokenType = oAuthResponse.tokenResponse.GetValue("token_type")?.ToString(),
            //    RefreshToken = oAuthResponse.tokenResponse.GetValue("refresh_token")?.ToString()
            //};
            //return response;
            return null;
        }

        #region Private Methods

        private void AddLoginHistory(LoginHistory model)
        {
            //burada oluşacak hatanın akışı kesmesini istemediğimiz için try catch kullandık
            try
            {
                if (model != null)
                    _loginHistoryDal.Add(model);
            }
            catch (Exception)
            {
            }
        }

        #endregion
    }
}
