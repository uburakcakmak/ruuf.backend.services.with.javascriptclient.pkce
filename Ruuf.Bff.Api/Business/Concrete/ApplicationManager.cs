﻿using Ruuf.Bff.Api.Business.Abstract;
using Ruuf.Bff.Api.DataAccess.Abstract;

namespace Ruuf.Bff.Api.Business.Concrete
{
    public class ApplicationManager : IApplicationService
    {
        #region DIs
        private readonly IInvestorApplicationDal _investorApplicationDal;

        public ApplicationManager(IInvestorApplicationDal investorApplicationDal)
        {
            _investorApplicationDal = investorApplicationDal;
        }

        #endregion

    }
}
