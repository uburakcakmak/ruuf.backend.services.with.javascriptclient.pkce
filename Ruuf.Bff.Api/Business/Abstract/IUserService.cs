﻿
using Ruuf.Entities.Concrete.IdentityServer;
using Ruuf.Entities.Concrete.RequestModels;
using Ruuf.Entities.Concrete.ViewModels;

namespace Ruuf.Bff.Api.Business.Abstract
{
    public interface IUserService
    {
        Task<string> RegisterUser(RegisterUserRequest registerUserRequest);

        Task<TokenResultViewModel> Login(UserLoginRequest loginRequest);

        Task<TokenResultViewModel> GetAccessTokenWithRefreshToken(RenewAccessTokenRequest renewAccessTokenRequest);

        Task Logout(string refreshToken);
    }
}
