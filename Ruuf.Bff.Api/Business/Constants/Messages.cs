﻿namespace Ruuf.Bff.Api.Business.Constants
{
    //TODO: ihtiyaca binaen bu kısmı(kullanıcı mesajlarını) dbden çekecek şekilde geliştirebiliriz. dbden çekme durumunda tüm liste dbden çekilip redis veya memcache cache'e atılmalı. her istekte dbye gidilmemeli
    public static class Messages
    {

        public static string UserNotFound = "Kullanıcı bulunamadı";
        public static string PasswordError = "Şifre hatalı";
        public static string SuccessfulLogin = "Sisteme giriş başarılı";
        public static string UserAlreadyExists = "Bu kullanıcı zaten mevcut";
        public static string UserRegistered = "Kullanıcı başarıyla kaydedildi";
        public static string AccessTokenCreated = "Access token başarıyla oluşturuldu";
        public static string ReferralCodeNotExists = "Hatalı referans kodu";
        public static string WrongApplicationType = "Hatalı başvuru türü seçildi";
        public static string UserRegisterError = "Kullanıcı eklenirken hata oluştu";

        public static string AuthorizationDenied = "Yetkiniz yok";
        public static string InvalidRefreshToken = "Geçersiz refresh token!";

        #region Validation Messages
        public static string InvalidEmailAddress = "Lütfen geçerli bir mail adresi giriniz.";
        public static string EmailIsRequired = "Mail adresi boş geçilemez";
        public static string PasswordEmpty = "Şifre boş geçilemez";
        public static string PasswordLength = "Şifre uzunluğu en az 8 karakter olmalı";
        public static string PasswordUppercaseLetter = "Şifre en az 1 büyük harf içermeli";
        public static string PasswordLowercaseLetter = "Şifre en az 1 küçük harf içermeli";
        public static string PasswordDigit = "Şifre en az 1 rakam içermeli";
        public static string PasswordSpecialCharacter = "Şifre en az 1 özel karakter içermeli";
        public static string FullNameIsRequired = "Ad-Soyad boş geçilemez";

        #endregion

    }
}
