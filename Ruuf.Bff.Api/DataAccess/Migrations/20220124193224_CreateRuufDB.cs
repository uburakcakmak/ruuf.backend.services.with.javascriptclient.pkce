﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ruuf.Bff.Api.DataAccess.Migrations
{
    public partial class CreateRuufDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "InvestorApplications",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "BINARY(16)", nullable: false),
                    UserId = table.Column<Guid>(type: "BINARY(16)", nullable: false),
                    InvestmentQuantity = table.Column<decimal>(type: "decimal(65,30)", nullable: false),
                    DeliveryAddress = table.Column<string>(type: "varchar(1000)", maxLength: 1000, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    OrderOfApplication = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    CreatedBy = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    UpdatedBy = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    UpdatedDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvestorApplications", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "LoginHistories",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "BINARY(16)", nullable: false),
                    UserId = table.Column<Guid>(type: "binary(16)", nullable: false),
                    IsLoginSuccessful = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsLoggedOut = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false),
                    IsMobileLogin = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false),
                    IsWebLogin = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false),
                    DeviceIp = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IsActive = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    CreatedBy = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    UpdatedBy = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    UpdatedDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoginHistories", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InvestorApplications");

            migrationBuilder.DropTable(
                name: "LoginHistories");
        }
    }
}
