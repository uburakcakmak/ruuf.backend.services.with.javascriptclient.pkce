﻿using Ruuf.Core.DataAccess;
using Ruuf.Entities.Concrete;

namespace Ruuf.Bff.Api.DataAccess.Abstract
{
    public interface ILoginHistoryDal : IEntityRepository<LoginHistory>
    {
    }
}
