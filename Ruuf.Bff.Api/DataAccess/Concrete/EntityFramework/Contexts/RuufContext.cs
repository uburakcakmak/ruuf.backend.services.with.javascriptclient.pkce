﻿using Microsoft.EntityFrameworkCore;
using Ruuf.Bff.Api.DataAccess.Concrete.EntityFramework.Mappings;
using Ruuf.Core.Utilities.Helpers;
using Ruuf.Entities.Concrete;

namespace Ruuf.Bff.Api.DataAccess.Concrete.EntityFramework.Contexts
{
    public class RuufContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = WebApplication.CreateBuilder();
            var mySqlConnectionStr = builder.Environment.IsProduction()
                ? AWSSecretHelper.GetDBConnString()
                : builder.Configuration.GetSection("ApiConfigurations:MySqlConnStr").Value;

            optionsBuilder.UseMySql(mySqlConnectionStr, ServerVersion.AutoDetect(mySqlConnectionStr));// Get the value from SELECT VERSION()
        }

        #region Models
        public DbSet<LoginHistory> LoginHistories { get; set; }
        public DbSet<InvestorApplication> InvestorApplications { get; set; }

        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration<LoginHistory>(new LoginHistoryMap())
                .Entity<LoginHistory>().ToTable("LoginHistories");

            modelBuilder.ApplyConfiguration<InvestorApplication>(new InvestorApplicationMap())
                .Entity<InvestorApplication>().ToTable("InvestorApplications");

        }
    }
}
