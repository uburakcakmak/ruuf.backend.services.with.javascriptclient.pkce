﻿using Ruuf.Bff.Api.DataAccess.Abstract;
using Ruuf.Bff.Api.DataAccess.Concrete.EntityFramework.Contexts;
using Ruuf.Core.DataAccess.EntityFramework;
using Ruuf.Entities.Concrete;

namespace Ruuf.Bff.Api.DataAccess.Concrete.EntityFramework
{
    public class EfInvestorApplicationDal : EfEntityRepositoryBase<InvestorApplication, RuufContext>, IInvestorApplicationDal
    {
    }
}
