﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Ruuf.Entities.Concrete;

namespace Ruuf.Bff.Api.DataAccess.Concrete.EntityFramework.Mappings
{
    public class InvestorApplicationMap : IEntityTypeConfiguration<InvestorApplication>
    {
        public void Configure(EntityTypeBuilder<InvestorApplication> builder)
        {
            builder.ToTable(@"InvestorApplication");
            builder.HasKey(u => u.Id);

            builder.Property(u => u.Id).HasColumnType("BINARY(16)");
            builder.Property(u => u.DeliveryAddress).IsRequired(true).HasMaxLength(1000);
            builder.Property(u => u.UserId).IsRequired(true).HasColumnType("BINARY(16)");
            builder.Property(u => u.InvestmentQuantity).IsRequired(true);
            builder.Property(u => u.UpdatedBy).IsRequired(false).HasMaxLength(100);

        }
    }
}
