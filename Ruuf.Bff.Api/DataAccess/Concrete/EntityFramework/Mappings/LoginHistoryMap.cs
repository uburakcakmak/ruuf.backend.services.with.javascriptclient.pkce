﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Ruuf.Entities.Concrete;

namespace Ruuf.Bff.Api.DataAccess.Concrete.EntityFramework.Mappings
{
    public class LoginHistoryMap : IEntityTypeConfiguration<LoginHistory>
    {
        public void Configure(EntityTypeBuilder<LoginHistory> builder)
        {
            builder.ToTable(@"LoginHistory");
            builder.HasKey(u => u.Id);

            builder.Property(u => u.Id).HasColumnType("BINARY(16)");
            builder.Property(u => u.IsLoggedOut).HasDefaultValue(false);
            builder.Property(u => u.CreatedBy).IsRequired(false).HasMaxLength(100);
            builder.Property(u => u.UpdatedBy).IsRequired(false).HasMaxLength(100);
        }
    }
}
