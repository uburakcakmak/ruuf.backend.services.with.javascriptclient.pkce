﻿using Autofac.Extensions.DependencyInjection;
using Autofac;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Ruuf.Core.Extensions;
using Ruuf.Core.Utilities.IoC;
using Ruuf.Core.DependencyResolvers;
using Microsoft.OpenApi.Models;
using Ruuf.Bff.Api.DataAccess.Concrete.EntityFramework.Contexts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Ruuf.Bff.Api.Business.DependencyResolvers.Autofac;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication;
using Ruuf.Core.Utilities.Configurations;
using Ruuf.Core.Utilities.Configurations.Swagger;
using Ruuf.Core.Utilities.Helpers;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();

#region Option Handlers
builder.Services.AddOptions();
builder.Services.Configure<ApiConfigurations>(builder.Configuration.GetSection(nameof(ApiConfigurations)));
var bffApiConfigurations = builder.Configuration.GetSection(nameof(ApiConfigurations)).Get<ApiConfigurations>();
bffApiConfigurations.MySqlConnStr = builder.Environment.IsProduction() ? AWSSecretHelper.GetDBConnString() : bffApiConfigurations.MySqlConnStr;

builder.Services.Configure<SwaggerConfigurations>(builder.Configuration.GetSection(nameof(SwaggerConfigurations)));
var swaggerConfigurations = builder.Configuration.GetSection(nameof(SwaggerConfigurations)).Get<SwaggerConfigurations>();

#endregion

#region Autofac

builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());

// Register services directly with Autofac here. Don't
// call builder.Populate(), that happens in AutofacServiceProviderFactory.
builder.Host.ConfigureContainer<ContainerBuilder>(builder => builder.RegisterModule(new AutofacBusinessModule()));
#endregion

#region Cors
builder.Services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
{
    builder
        .SetIsOriginAllowed(origin => new Uri(origin).Host == "localhost") // Environment'i değiştirsek bile, localden debug yapabilmek için izin verildi
        .WithOrigins(bffApiConfigurations.AllowedCORSOrigins?.Replace(" ", "").Split(','))
        .AllowAnyMethod()
        .AllowAnyHeader()
        .AllowCredentials();
}));

#endregion

#region Redis

builder.Services.AddStackExchangeRedisCache(action =>
{
    action.Configuration = bffApiConfigurations.RedisConnStr;
});

#endregion

#region Identity Server Auth

builder.Services.AddAuthentication(_ => new AuthenticationOptions
{
    DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme,
    DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme
}).AddJwtBearer(_ => new JwtBearerOptions
{
    Authority = bffApiConfigurations.AuthApiAuthority,
    Audience = bffApiConfigurations.SelfApiName,
    RequireHttpsMetadata = false,
    SaveToken = true, 
    TokenValidationParameters = new TokenValidationParameters
    {
        ValidateAudience = true,
        ValidIssuer = bffApiConfigurations.AuthApiAuthority,
        ValidateIssuer = true,
        ValidateLifetime = true,
        ClockSkew = TimeSpan.Zero
    }
});

builder.Services.AddAuthorization(config =>
{
    config.DefaultPolicy = new AuthorizationPolicyBuilder() //anonim belirtilmediği sürece tüm endpointler authenticate ister
                .RequireAuthenticatedUser()
                .RequireClaim("Role") // her istekte mutlaka kullanıcının rol claim'i olmalı
                .Build();
});

#endregion

#region Db Auto Migration at Startup

var ruufDbContext = new RuufContext();
ruufDbContext.Database.Migrate();

#endregion

#region Service Collection Extensions

builder.Services.AddDependencyResolvers(new ICoreModule[] //Dependency Injection'ı merkezileştiriyoruz
{
    new CoreModule()
});

builder.Services.AddRuufSwaggerGen(
    new OpenApiInfo
    {
        Version = swaggerConfigurations.SwaggerDoc.Version,
        Title = swaggerConfigurations.SwaggerDoc.Title,
        Description = swaggerConfigurations.SwaggerDoc.Description
    }, new OpenApiContact
    {
        Name = swaggerConfigurations.SwaggerDoc.Contact.Name,
        Email = swaggerConfigurations.SwaggerDoc.Contact.Email,
        Url = swaggerConfigurations.SwaggerDoc.Contact.Url
    });
#endregion

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddHttpClient();
builder.Services.AddEndpointsApiExplorer();

var app = builder.Build();
// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment()) ileride gerekli olabileceği için kaldırılmadı
//{
app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint(
        String.Format(swaggerConfigurations.UseSwaggerUI.SwaggerUIEndpoint, swaggerConfigurations.SwaggerDoc.Version),
                      swaggerConfigurations.UseSwaggerUI.UIName
        );
});
//}

//app.ConfigureCustomExceptionMiddleware(); bu kısmı geliştireceğim. TODO.txt içerisine not aldım

app.UseCors("CorsPolicy");
//app.UseHttpsRedirection();
app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

var options = app.Services.GetService<IOptions<RequestLocalizationOptions>>();
app.UseRequestLocalization(options.Value);

app.MapControllers();
app.Run();
