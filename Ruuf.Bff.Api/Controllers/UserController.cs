﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Ruuf.Bff.Api.Business.Abstract;
using Ruuf.Bff.Api.Business.Constants;
using Ruuf.Core.Utilities.Helpers;
using Ruuf.Entities.Concrete.IdentityServer;
using Ruuf.Entities.Concrete.RequestModels;
using Ruuf.Entities.Concrete.ViewModels;

namespace Ruuf.Bff.Api.Controllers
{
    //[ApiController]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Yatırımcı ve Ev için başvuracak kişinin kullanıcı kaydı bu endpoint ile yapılır. Başvuru tipi Yatırımcı için 0, Ev için 1 gönderilmeli
        /// </summary>
        /// <param name="registerUserRequest"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("RegisterUser")]
        public async Task<string> RegisterUser([FromBody] RegisterUserRequest registerUserRequest)
        {
            return await _userService.RegisterUser(registerUserRequest);
        }

        /// <summary>
        /// Gönderilen kullanıcı bilgileri doğruysa access token döndürür. Token aldıktan sonra tüm istekler token ile yapılmalı 
        /// </summary>
        /// <param name="loginRequest"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("Login")]
        public async Task<TokenResultViewModel> Login([FromBody] UserLoginRequest loginRequest)
        {
            var userToLogin = await _userService.Login(loginRequest);
            if (userToLogin == null)
                throw new Exception(Messages.AuthorizationDenied);

            return userToLogin;
        }

        [HttpGet("Logout")]
        public async Task Logout([FromQuery] string refreshToken)
        {
            await _userService.Logout(refreshToken);
        }

        [AllowAnonymous]
        [HttpGet("GetAccessTokenWithRefreshToken")]
        public async Task<TokenResultViewModel> GetAccessTokenWithRefreshToken([FromBody] RenewAccessTokenRequest renewAccessTokenRequest)
        {
            if (string.IsNullOrEmpty(renewAccessTokenRequest.RefreshToken))
                throw new Exception(Messages.InvalidRefreshToken);

            var result = await _userService.GetAccessTokenWithRefreshToken(renewAccessTokenRequest);
            if (result != null)
                throw new Exception(Messages.InvalidRefreshToken);

            return result;
        }

        [AllowAnonymous]
        [HttpGet("GetUser")]
        public string GetUser()
        {
            var a = User.Claims;
            return "BurAK";
        }

    }
}
