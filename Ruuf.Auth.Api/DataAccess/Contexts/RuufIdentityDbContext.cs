﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Ruuf.Core.Extensions;
using Ruuf.Entities.Concrete.IdentityServer;

namespace Ruuf.IdentityServer.DataAccess.Contexts
{
    public class RuufIdentityDbContext : IdentityDbContext<User, Role, string>
    {
        public RuufIdentityDbContext(DbContextOptions<RuufIdentityDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.SeedRoles();
            base.OnModelCreating(modelBuilder);
        }
    }
}