﻿using IdentityServer4.EntityFramework.Entities;
using IdentityServer4.EntityFramework.Extensions;
using IdentityServer4.EntityFramework.Interfaces;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Ruuf.Entities.Concrete.IdentityServer;

namespace Ruuf.IdentityServer.DataAccess.Contexts
{
    public class RuufPersistedGrantDbContext : DbContext, IPersistedGrantDbContext
    {
        private readonly OperationalStoreOptions _storeOptions;

        public RuufPersistedGrantDbContext(DbContextOptions<RuufPersistedGrantDbContext> options, OperationalStoreOptions storeOptions)
            : base(options)
        {
            _storeOptions = storeOptions;
        }

        public DbSet<PersistedGrant> PersistedGrants { get; set; }
        public DbSet<DeviceFlowCodes> DeviceFlowCodes { get; set; }
        public DbSet<ClientEndpointPermission> ClientEndpointPermissions { get; set; }

        public Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ConfigurePersistedGrantContext(_storeOptions);
            modelBuilder.Entity<ClientEndpointPermission>().ToTable("IdSrv_Persisted_" + nameof(ClientEndpointPermission));

            base.OnModelCreating(modelBuilder);
        }
    }
}