﻿using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.EntityFramework.Options;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Ruuf.Core.Utilities.Configurations;
using Ruuf.Entities.Concrete.IdentityServer;
using Ruuf.IdentityServer.DataAccess.Contexts;

namespace Ruuf.IdentityServer.Utilities
{
    public static class Extensions
    {
        public static ServerVersion _mySqlVersion { get; set; }
        public static string _migrationAssemblyName { get; set; } = typeof(Program).Assembly.GetName().Name;

        /// <summary>
        /// Determines whether the client is configured to use PKCE.
        /// </summary>
        /// <param name="store">The store.</param>
        /// <param name="client_id">The client identifier.</param>
        /// <returns></returns>
        public static async Task<bool> IsPkceClientAsync(this IClientStore store, string client_id)
        {
            if (!string.IsNullOrWhiteSpace(client_id))
            {
                var client = await store.FindEnabledClientByIdAsync(client_id);
                return client?.RequirePkce == true;
            }

            return false;
        }

        public static IServiceCollection AddIdentityServerConfig(this IServiceCollection services, AuthApiConfigurations authApiConfigurations)
        {
            _mySqlVersion = ServerVersion.AutoDetect(authApiConfigurations.MySqlConnStr);
            services.AddIdentity<User, Role>(options =>
            {
                options.User.RequireUniqueEmail = true;
                options.Password.RequiredLength = 8;
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequireLowercase = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireDigit = true;
                options.Password.RequireNonAlphanumeric = true;
                options.User.AllowedUserNameCharacters = "abcçdefghiıjklmnoöpqrsştuüvwxyzABCÇDEFGHIİJKLMNOÖPQRSŞTUÜVWXYZ0123456789-._@+'#!/^%{}*";
            }).AddEntityFrameworkStores<RuufIdentityDbContext>()
              .AddDefaultTokenProviders();

            services.AddIdentityServer()
                    .AddDeveloperSigningCredential()
                    .AddOperationalStore(options =>
                    {
                        foreach (var p in options.GetType().GetProperties())
                        {
                            if (p.PropertyType == typeof(TableConfiguration))
                            {
                                var o = p.GetGetMethod().Invoke(options, null);
                                var q = o.GetType().GetProperty("Name");

                                var tableName = q.GetMethod.Invoke(o, null) as string;
                                o.GetType().GetProperty("Name").SetMethod.Invoke(o, new object[] { $"IdSrv_Opr_{tableName}" });
                            }
                        }

                        options.ConfigureDbContext = b =>
                        {
                            b.UseMySql(authApiConfigurations.MySqlConnStr, _mySqlVersion);
                        };
                        // this enables automatic token cleanup. this is optional.
                        options.EnableTokenCleanup = true;
                    })
                    .AddConfigurationStore(options =>
                    {
                        // Loop through and rename each table to 'IdSrvConf{tablename}' - E.g. `IdSrvConfApiResources`
                        foreach (var p in options.GetType().GetProperties())
                        {
                            if (p.PropertyType == typeof(TableConfiguration))
                            {
                                var o = p.GetGetMethod().Invoke(options, null);
                                var q = o.GetType().GetProperty("Name");

                                var tableName = q.GetMethod.Invoke(o, null) as string;
                                o.GetType().GetProperty("Name").SetMethod.Invoke(o, new object[] { $"IdSrv_Conf_{tableName}" });
                            }
                        }
                        options.ConfigureDbContext = b =>
                        {
                            b.UseMySql(authApiConfigurations.MySqlConnStr, _mySqlVersion);
                        };
                    })
                .AddAspNetIdentity<User>();
            return services;
        }

        public static IServiceCollection AddISServices<TUser>(this IServiceCollection services) where TUser : IdentityUser, new()
        {
            services.AddTransient<IProfileService, RuufProfileService>();
            return services;
        }

        public static IServiceCollection AddDatabaseConfiguration(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<RuufIdentityDbContext>(options =>
                options.UseMySql(connectionString, _mySqlVersion,
                    x =>
                    {
                        x.MigrationsAssembly(_migrationAssemblyName);
                        x.MigrationsHistoryTable("Ef_Migrations_History_IdSrv_Identity");
                    }
            ));
            services.AddDbContext<RuufPersistedGrantDbContext>(options =>
                options.UseMySql(connectionString, _mySqlVersion,
                    x =>
                    {
                        x.MigrationsAssembly(_migrationAssemblyName);
                        x.MigrationsHistoryTable("Ef_Migrations_History_IdSrv_Opr");
                    }
            ));
            services.AddDbContext<RuufConfigurationDbContext>(options =>
                options.UseMySql(connectionString, _mySqlVersion,
                    x =>
                    {
                        x.MigrationsAssembly(_migrationAssemblyName);
                        x.MigrationsHistoryTable("Ef_Migrations_History_IdSrv_Conf");
                    }
            ));

            return services;
        }

        public static void AddDatabaseMigrations(this IServiceProvider services, AuthApiConfigurations authApiConfigurations)
        {
            using (var serviceScope = services.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetRequiredService<RuufIdentityDbContext>().Database.Migrate();
                serviceScope.ServiceProvider.GetRequiredService<RuufPersistedGrantDbContext>().Database.Migrate();

                var configContext = serviceScope.ServiceProvider.GetRequiredService<RuufConfigurationDbContext>();
                configContext.Database.Migrate();

                if (authApiConfigurations.IdentityResources != null)
                {
                    foreach (var resource in authApiConfigurations.IdentityResources)
                    {
                        if (!configContext.IdentityResources.Any(x => x.Name == resource.Name))
                        {
                            configContext.IdentityResources.Add(resource.ToEntity());
                        }
                    }

                    configContext.SaveChanges();
                }

                if (authApiConfigurations.ApiResources != null)
                {
                    foreach (var resource in authApiConfigurations.ApiResources)
                    {
                        if (!configContext.ApiResources.Any(x => x.Name == resource.Name))
                        {
                            configContext.ApiResources.Add(resource.ToEntity());
                        }
                    }

                    configContext.SaveChanges();
                }

                if (authApiConfigurations.ApiScopes != null)
                {
                    foreach (var scope in authApiConfigurations.ApiScopes)
                    {
                        if (!configContext.ApiScopes.Any(x => x.Name == scope.Name))
                        {
                            configContext.ApiScopes.Add(scope.ToEntity());
                        }
                    }

                    configContext.SaveChanges();
                }

                if (authApiConfigurations.Clients != null)
                {
                    foreach (var client in authApiConfigurations.Clients)
                    {
                        if (!configContext.Clients.Any(x => x.ClientId == client.ClientId))
                        {
                            configContext.Clients.Add(client.ToEntity());
                        }
                    }

                    configContext.SaveChanges();
                }
            }
        }

    }
}
