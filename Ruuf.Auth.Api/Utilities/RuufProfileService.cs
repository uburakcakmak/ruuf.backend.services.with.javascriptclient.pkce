﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using Ruuf.Core.Extensions;
using Ruuf.Entities.Concrete.IdentityServer;
using System.Security.Claims;

namespace Ruuf.IdentityServer.Utilities
{
    public class RuufProfileService : IProfileService
    {
        private readonly UserManager<User> _userManager;

        public RuufProfileService(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        //Get user profile date in terms of claims when calling /connect/userinfo
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            try
            {
                var userId = context.Subject.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Subject)?.Value;
                if (!string.IsNullOrWhiteSpace(userId))
                {
                    var claims = (await GetUserClaims(userId)).ToList();

                    foreach (var item in context.Subject.Claims)
                    {
                        if (!claims.Any(x => x.Type == item.Type))
                        {
                            claims.Add(item);
                        }
                    }

                    await Task.Run(() => context.IssuedClaims = claims);
                }
                else
                {
                    await Task.Run(() => context.IssuedClaims = context.Subject.Claims.ToList());
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            try
            {
                var userId = context.Subject.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Subject)?.Value;
                if (!string.IsNullOrWhiteSpace(userId))
                {
                    var user = await GetUser(userId);
                    if (user != null)
                    {
                        await Task.Run(() => context.IsActive = true);
                        return;
                    }
                }
                await Task.Run(() => context.IsActive = false);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        private async Task<IEnumerable<Claim>> GetUserClaims(string userId)
        {
            var user = await GetUser(userId);
            var userRoles = await GetUserRoles(user);
            var claims = new List<Claim>();

            if (user != null)
            {
                claims.AddNameIdentifier(user?.Id.ToString());
                claims.AddName(user?.FullName);
                claims.AddRoles(userRoles?.ToArray());
            }
            return claims ?? null;
        }

        private async Task<User> GetUser(string userId)
        {
            return await _userManager.FindByIdAsync(userId);
        }

        private async Task<List<string>> GetUserRoles(User user)
        {
            return (await _userManager.GetRolesAsync(user)).ToList();
        }
    }
}
