﻿using Ruuf.Core.Utilities.Configurations;
using Ruuf.Core.Utilities.Helpers;
using Ruuf.Entities.Concrete.IdentityServer;
using Ruuf.IdentityServer.Utilities;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();

#region Option Handlers
builder.Services.AddOptions();

//builder.Services.ConfigureRuufServiceSettings<IdentityServerConfigurations>(configuration); // TODO: bu extension'ı yeni yazdım. henüz testlerini yapmadım. aşağıdakiyle aynı işi daha kolay yapmamızı sağlıyor
builder.Services.Configure<AuthApiConfigurations>(builder.Configuration.GetSection(nameof(AuthApiConfigurations)));
var authApiConfigurations = builder.Configuration.GetSection(nameof(AuthApiConfigurations)).Get<AuthApiConfigurations>();
authApiConfigurations.MySqlConnStr = builder.Environment.IsProduction() ? AWSSecretHelper.GetDBConnString() : authApiConfigurations.MySqlConnStr;

#endregion

#region Identity Server Config

//var secret = new Secret("secret".Sha256());
builder.Services
              .AddDatabaseConfiguration(authApiConfigurations.MySqlConnStr)
              .AddIdentityServerConfig(authApiConfigurations)
              .AddISServices<User>();

#endregion

builder.Services.ConfigureApplicationCookie(config =>
{
    config.Cookie.Name = "RuufAuthApi.Cookie";
    config.LoginPath = "/Auth/Login";
    config.LogoutPath = "/Auth/Logout";
});
builder.Services.AddControllersWithViews();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

#region IS Contexts Auto Migrations at Startup

app.Services.AddDatabaseMigrations(authApiConfigurations);

#endregion

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();
app.UseRouting();

app.UseIdentityServer();

app.UseCookiePolicy(new CookiePolicyOptions()
{
    MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.Lax
});

app.UseEndpoints(endpoints =>
{
    endpoints.MapDefaultControllerRoute();
});

app.Run();
