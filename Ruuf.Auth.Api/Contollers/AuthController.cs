﻿using System.Security.Claims;
using IdentityServer.Controllers;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Ruuf.Core.Utilities.Helpers;
using Ruuf.Entities.Concrete.IdentityServer;
using Ruuf.Entities.Concrete.RequestModels;
using Ruuf.Entities.Enums;
using Ruuf.IdentityServer.Utilities.Constants;

namespace Ruuf.IdentityServer.Contollers
{
    public class AuthController : Controller
    {
        #region DIs
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly IIdentityServerInteractionService _interactionService;

        public AuthController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IIdentityServerInteractionService interactionService)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _interactionService = interactionService;
        }

        #endregion

        [HttpGet]
        public async Task<IActionResult> Logout(string logoutId)
        {
            await _signInManager.SignOutAsync();

            var logoutRequest = await _interactionService.GetLogoutContextAsync(logoutId);

            if (string.IsNullOrEmpty(logoutRequest.PostLogoutRedirectUri))
            {
                return RedirectToAction("Index", "Home");
            }

            return Redirect(logoutRequest.PostLogoutRedirectUri);
        }

        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl)
        {
            return View(new LoginViewModel
            {
                ReturnUrl = returnUrl,
            });
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel vm)
        {

            // check if the model is valid

            var result = await _signInManager.PasswordSignInAsync(vm.Username, vm.Password, false, false);

            if (result.Succeeded)
            {
                return Redirect(vm.ReturnUrl);
            }
            else if (result.IsLockedOut)
            {

            }

            return View();
        }

        [HttpGet]
        public IActionResult Register(string returnUrl)
        {
            return View(new RegisterViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        //[HttpPost("/api/Auth/Register")]
        public async Task<IActionResult> Register(RegisterUserRequest registerUserRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //kullanıcı yatırımcı veya ev için başvuruyorsa ve ba
            //şvuru türü yatırımcı ve ev için değilse hata döneceğiz
            if ((registerUserRequest.Role == Roles.ApplicantForHome || registerUserRequest.Role == Roles.Investor) &&
                (registerUserRequest.ApplicationType != ApplicationTypes.Investor && registerUserRequest.ApplicationType != ApplicationTypes.ApplicantForHome))
                throw new Exception(Messages.WrongApplicationType);

            var user = await GetUserByEmail(registerUserRequest.Email);
            if (user != null)
                throw new Exception(Messages.UserAlreadyExists);

            var referralCode = await GetByReferralCode(registerUserRequest.ReferredBy);
            if (referralCode != null)
                throw new Exception(Messages.ReferralCodeNotExists);

            var newUser = new User
            {
                Email = registerUserRequest.Email,
                FullName = registerUserRequest.FullName,
                IdentificationNumber = registerUserRequest.IdentificationNumber,
                IncomeSource = registerUserRequest.IncomeSource,
                IsRDWAAllowed = registerUserRequest.IsRDWAAllowed,
                IsCECPAllowed = registerUserRequest.IsCECPAllowed,
                IsPDPLPPAllowed = registerUserRequest.IsPDPLPPAllowed,
                IsSmsNotificationAllowed = registerUserRequest.IsSmsNotificationAllowed,
                IsEmailNotificationAllowed = registerUserRequest.IsEmailNotificationAllowed,
                IsPhoneCallAllowed = registerUserRequest.IsPhoneCallAllowed,
                NameOfWorkPlace = registerUserRequest.NameOfWorkPlace,
                OfficeAddress = registerUserRequest.OfficeAddress,
                PhoneNumber = registerUserRequest.PhoneNumber,
                Profession = registerUserRequest.Profession,
                ReferredBy = registerUserRequest.ReferredBy,
                ReferralCode = RandomGenerator.UpperCaseAndNumber(6),
                IsActive = true,
                CreatedDate = DateTime.Now,
                UserName = registerUserRequest.Email,
                UpdatedBy = string.Empty
            };

            // add user
            var result = await _userManager.CreateAsync(newUser, registerUserRequest.Password);
            if (!result.Succeeded)
                return BadRequest(result.Errors);

            //add role to user
            var addedUser = await GetUserByEmail(newUser.Email);
            var userRole = await _userManager.AddToRoleAsync(addedUser, registerUserRequest.Role.ToString());
            if (!userRole.Succeeded)
                return BadRequest(userRole.Errors);

            await _userManager.AddClaimAsync(addedUser, new Claim("user_id", addedUser.Id));
            await _userManager.AddClaimAsync(addedUser, new Claim("full_name", addedUser.FullName));
            await _userManager.AddClaimAsync(addedUser, new Claim("role", registerUserRequest.Role.ToString()));

            return Ok(Messages.UserRegistered);
        }

        private async Task<User> GetUserByEmail(string emailAddress)
        {
            var user = await _userManager.FindByEmailAsync(emailAddress);
            return user;
        }

        private async Task<User> GetByReferralCode(string referralCode)
        {
            // aşağıdaki lambda expressiondaki "_" discard operatörüdür ve tüm be lambda expressionlarda böyle tanımlanmalıdır.
            // Discard operatörü yerine değişken tanımlanırsa bellekte yer tutar, bu da performansı etkiler. ama discard operatörü bellekte yer tutmaz
            // benden başka be kodlayacaklar için bu da dipnot olsun :)
            var user = await _userManager.Users.Where(_ => _.ReferralCode == referralCode).FirstOrDefaultAsync();
            return user ?? null;
        }
    }
}