﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ruuf.Entities.Enums
{
    public enum Roles
    {
        Admin,
        Reviewer,
        Investor,
        ApplicantForHome
    }
}
