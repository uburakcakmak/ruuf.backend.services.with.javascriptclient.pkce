﻿
namespace Ruuf.Entities.Enums
{
    public enum RequestStatus
    {
        New,
        WaitingForReviewerApproval,
        WaitingForManagerApproval,
        Approved,
        Rejected,
        Canceled
    }
}
