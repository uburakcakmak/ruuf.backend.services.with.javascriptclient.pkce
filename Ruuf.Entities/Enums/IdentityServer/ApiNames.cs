﻿namespace Ruuf.Entities.Enums.IdentityServer
{
    public enum ApiNames
    {
        RuuftBffApi,
        RuufNotificationApi,
        RuufIdentityServer
    }
}
