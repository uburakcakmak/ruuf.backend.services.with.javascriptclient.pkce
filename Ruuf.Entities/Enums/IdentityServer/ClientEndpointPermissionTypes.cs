﻿namespace Ruuf.Entities.Enums.IdentityServer
{
    public enum ClientEndpointPermissionTypes
    {
        Allow = 1,
        Deny = 0
    }
}
