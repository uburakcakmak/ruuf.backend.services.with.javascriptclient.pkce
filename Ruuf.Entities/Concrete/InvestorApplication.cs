﻿using Ruuf.Entities.Concrete.BaseEntities;

namespace Ruuf.Entities.Concrete
{
    public class InvestorApplication : BaseEntity
    {
        public Guid UserId { get; set; }
        public decimal InvestmentQuantity { get; set; }
        public string DeliveryAddress { get; set; }
        public int OrderOfApplication { get; set; }
    }
}
