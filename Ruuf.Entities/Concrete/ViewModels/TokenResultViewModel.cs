﻿
namespace Ruuf.Entities.Concrete.ViewModels
{
    public class TokenResultViewModel
    {
        public string AccessToken { get; set; }
        public string AccessTokenExpiresIn { get; set; }
        public string TokenType { get; set; }
        public string Scope { get; set; }
        public string RefreshToken { get; set; }
    }
}
