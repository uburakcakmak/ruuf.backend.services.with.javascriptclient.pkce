﻿
namespace Ruuf.Entities.Concrete.RequestModels
{
    public class RenewAccessTokenRequest
    {
        public string RefreshToken { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
