
namespace Ruuf.Entities.Concrete.IdentityServer
{
    public class UserLoginRequest : IDto
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string? ReturnUrl { get; set; }

        public string? DeviceIp { get; set; }

        public string? DeviceName { get; set; }

    }
}