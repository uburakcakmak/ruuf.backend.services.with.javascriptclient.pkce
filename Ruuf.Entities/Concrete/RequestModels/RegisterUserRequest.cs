﻿using Ruuf.Entities.Enums;
using System.ComponentModel;

namespace Ruuf.Entities.Concrete.RequestModels
{
    public class RegisterUserRequest : IDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }

        //Kimlik numarası
        public string IdentificationNumber { get; set; }

        //Telefon numarası
        public string PhoneNumber { get; set; }

        //Sms izni
        public bool IsSmsNotificationAllowed { get; set; }

        //Telefon arama izni
        public bool IsPhoneCallAllowed { get; set; }

        //E-posta izni
        public bool IsEmailNotificationAllowed { get; set; }

        [DisplayName("Kişisel Verileri Koruma Kanunu İzni Gizlilik Politikası|Personal Data Protection Law Privacy Policy")]
        public bool IsPDPLPPAllowed { get; set; }

        [DisplayName("Ticari Elektronik İletişim İzni|Commercial Electronic Communication Permit")]
        public bool IsCECPAllowed { get; set; }

        [DisplayName("Risk Beyanı ve Feragatı Sözleşmesi|Risk Disclosure and Waiver Agreement")]
        public bool IsRDWAAllowed { get; set; }

        //Gelir Kaynağı
        public string IncomeSource { get; set; }

        //Meslek
        public string Profession { get; set; }

        //Çalışılan yerin adı
        public string NameOfWorkPlace { get; set; }

        //İşyeri adresi, workplaceaddress
        public string OfficeAddress { get; set; }

        //Referans olan kişi
        public string ReferredBy { get; set; }

        public ApplicationTypes ApplicationType { get; set; }

        public Roles Role { get; set; }

        public string ReturnUrl { get; set; }

    }
}
