﻿using Ruuf.Entities.Concrete.BaseEntities;

namespace Ruuf.Entities.Concrete
{
    public class LoginHistory : BaseEntity
    {
        public Guid UserId { get; set; }

        public bool IsLoginSuccessful { get; set; }

        public bool IsLoggedOut { get; set; }

        public string DeviceIp { get; set; } // sisteme hangi ip adresiyle giriş yapılmış bilgisini tutacağız. farklı ip adresiyle giriş yapıldığında bilgilendirme maili göndereceğiz

    }
}
