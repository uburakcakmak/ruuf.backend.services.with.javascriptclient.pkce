﻿
namespace Ruuf.Entities.Concrete.BaseEntities
{
    /// <summary>
    /// All entities should inherit this class
    /// </summary>
    public class BaseEntity : IEntity
    {
        public Guid Id { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
