﻿using Ruuf.Entities.Concrete.BaseEntities;
using Ruuf.Entities.Enums.IdentityServer;

namespace Ruuf.Entities.Concrete.IdentityServer
{
    public class ClientEndpointPermission : BaseEntity
    {
        public string ClientId { get; set; }
        public ApiNames ApiName { get; set; }
        public string ControllerNameRegex { get; set; }
        public string ActionNameRegex { get; set; }
        public ClientEndpointPermissionTypes PermissionType { get; set; }
    }
}

