﻿using Microsoft.AspNetCore.Identity;

namespace Ruuf.Entities.Concrete.IdentityServer
{
    public class Role : IdentityRole, IEntity
    {
    }
}